package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/linhnta/firstprojectwithentgo/ent"
	pb "gitlab.com/linhnta/firstprojectwithentgo/protos"
)

const (
	port = ":50051"
)

type UserServer struct {
	pb.UnimplementedUserServer
	db *ent.Client
}

func main() {

	client, err := ent.Open("mysql", "root:admin@tcp(localhost:3306)/TEST?parseTime=True")
	fmt.Printf("oke: %v", client)
	if err != nil {
		log.Fatalf("failed opening connection to mysql: %v", err)
	}
	defer client.Close()
	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()

	pb.RegisterUserServer(s, &UserServer{pb.UnimplementedUserServer{}, client})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}

func (s *UserServer) GetUser(ctx context.Context, in *pb.GetUserRequest) (*pb.GetUserReply, error) {
	user, err := s.db.User.Get(ctx, in.GetId())

	if err != nil {
		return nil, fmt.Errorf("error user not found : %v", err)
	}

	return &pb.GetUserReply{
		UserInfo: &pb.UserInfo{
			Id:   user.ID,
			Name: user.Name,
			Age:  user.Age,
		},
	}, nil
}

func (s *UserServer) CreateUser(ctx context.Context,
	in *pb.CreateUserRequest) (*pb.CreateUserReply, error) {

	user, err := s.db.User.Create().
		SetName(in.GetName()).
		SetAge(in.GetAge()).
		Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("error ... : %v", err)
	}

	return &pb.CreateUserReply{
		UserInfo: &pb.UserInfo{
			Id:   user.ID,
			Name: user.Name,
			Age:  user.Age,
		},
	}, nil
}

func (s *UserServer) UpdateUser(ctx context.Context,
	in *pb.UpdateUserRequest) (*pb.UpdateUserReply, error) {
	log.Printf("Received: %v", in)

	user, err := s.db.User.UpdateOneID(in.GetId()).
		SetName(in.GetName()).
		SetAge(in.GetAge()).
		Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("error ... : %v", err)
	}

	return &pb.UpdateUserReply{
		UserInfo: &pb.UserInfo{
			Id:   user.ID,
			Name: user.Name,
			Age:  user.Age,
		},
	}, nil
}

func (s *UserServer) DeleteUser(ctx context.Context, in *pb.DeleteUserRequest) (*pb.DeleteUserReply, error) {
	err := s.db.User.DeleteOneID(in.GetId()).Exec(ctx)
	log.Printf("Received: %v", in.GetId())

	if err != nil {
		return nil, fmt.Errorf("failed deleting user: %v", err)
	}

	return &pb.DeleteUserReply{St: "success"}, nil
}

func (s *UserServer) ListUsers(ctx context.Context, in *pb.ListUsersRequest) (*pb.ListUsersReply, error) {
	users, err := s.db.User.Query().All(ctx)

	if err != nil {
		return nil, fmt.Errorf("failed deleting user: %v", err)
	}

	var res pb.ListUsersReply
	for _, user := range users {
		res.UserInfo = append(res.UserInfo, &pb.UserInfo{
			Id:   user.ID,
			Name: user.Name,
			Age:  user.Age,
		})

	}
	return &res, nil

}
