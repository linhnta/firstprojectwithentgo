#build stage
FROM golang:latest

RUN mkdir /app

WORKDIR /app

COPY . /app/firstprojectwithentgo/

RUN cd /app/firstprojectwithentgo && go mod download all

RUN cd /app/firstprojectwithentgo/server && go build

EXPOSE 50051


CMD ["/app/firstprojectwithentgo/server/server"]